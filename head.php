<head>
    <!-- Google Tag Manager -->
    <script async>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-T868PHZ');
    </script>
    <!-- End Google Tag Manager -->
    <title>Inky Digital</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Soluções Tecnológicas que Transformam!">
    <meta name="author" content="Inky Digital">
    <meta name="keywords"
          content="Website, mobile, aplicações, web, inky digital, criar website, criar aplicativo, bot, desenvolvimento, software, aplicativo, app, android, ios, personalizado, site, inteligencia artificial, automação, nativo, react, wordpress, modelagem 3D, 3D, identidade visual, marca, logo, social media, redes sociais, gerenciamento, design gráfico">
    <meta name="robots" content="index">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="pt-BR">
    <meta name="generator" content="N/A">
    <meta charset="utf-8">
    <link rel="stylesheet" href='css/inky-base.min.css'>
    <link rel="stylesheet" href='css/mobile-menu.css'>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="css/theme.min.css">
    <link rel="shortcut icon" href="img/logo-icon.svg">

    <link rel="preload" href="css/bootstrap.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </noscript>
    <link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" as="style"
          onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    </noscript>
    <link rel="preload" href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap"
          as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap">
    </noscript>

    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css"/>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-194032811-1">
    </script>
    <script async>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-194032811-1');
    </script>
    <script async>
        window.addEventListener("load", function () {
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "rgba(80, 78, 93, 0.40)",
                        "text": "#fff",
                    },
                    "button": {
                        "background": "#4B28F7",
                    }
                },
                "position": "bottom-right",
                "content": {
                    "message": "Esse website utiliza cookies para garantir que você tenha a melhor experiência possível enquanto navega.",
                    "dismiss": "Entendi!",
                    "link": "Saiba Mais",
                    "href": "politica-de-privacidade"
                }
            })
        });
    </script>
    <script defer>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 2367640, hjsv: 6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
</head>