//Mobile Menu

function openNav(element) {
    var opened = element.getAttribute('opened');

    if (opened == 0) {
        document.getElementById("mySidenav").classList.add("active");
        element.setAttribute('opened', 1);
    } else {
        document.getElementById("mySidenav").classList.remove("active");
        element.setAttribute('opened', 0);
    }
}

function closeNav() {
    document.getElementById("mySidenav").classList.remove("active");
    document.getElementById("menu-btn").classList.remove("open");
}