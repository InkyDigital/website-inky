$(document).ready(function() {
    // initiate full page scroll
    $('#fullpage').fullpage({
        responsiveWidth: 1000,
        sectionSelector: '.inky-scrolling',
        slideSelector: '.inky-carousel-scrolling',
        navigation: false,
        slidesNavigation: true,
        controlArrows: false,


        afterLoad: function ( anchorLink, index, div, ){
            var loadedSection = $(this);


            //using index
            if(index===1){
                $('.header-links a').each(function(){
                    $(this).css('color','#bbbdc1')
                });

                $('.header-links .active').each(function(){
                    $(this).css('color','#253141')
                });

                $('.header-links .icone-color').each(function(){
                    $(this).css('color','#253141')
                });

                $('.header-links .inky-logo-content').each(function(){
                    $(this).css('background-image','url(\'img/logo.svg\')')
                });
                $('.header-links').css("background-color","transparent");
            }

            else if(index!==1){
                $('.header-links a').each(function(){
                    $(this).css('color','#929292')
                });

                $('.header-links .active').each(function(){
                    $(this).css('color','white')
                });

                $('.header-links .icone-color').each(function(){
                    $(this).css('color','#929292')
                });

                $('.header-links .inky-logo-content').each(function(){
                    $(this).css('background-image','url(\'img/logo-white.svg\')')
                });
                $('.header-links').css('background-color', 'rgba(21, 25, 33 ,0.61)');
            }

            //using index
            if(index === 2){

                /* animate skill bars */
                $('.skillbar').each(function(){
                    $(this).find('.skillbar-bar').animate({
                        width:jQuery(this).attr('data-percent')
                    },2500);
                });
            }
        }
    });

    // smooth scrolling
    $('a[href*=\\#]:not([href=\\#])').click(function() {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

});
