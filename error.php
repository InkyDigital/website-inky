<?php


?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Inky Digital | Página não encontrada!</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Soluções Tecnológicas que Transformam!">
    <meta name="keywords"
          content="Website, aplicativos, apps, mobile, aplicações, web, aplicações móveis,">
    <meta name="language" content="Portuguese">
    <meta name="generator" content="N/A">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="shortcut icon" href="img/logo-icon.svg">
    <link rel="stylesheet" href="css/erro.css">
</head>
<body class="bg-purple">

<div class="stars">

    <div class="central-body">
        <div class="text-content">
            <a href="inicio">
                <img src="img/logo-white.svg" alt="Logo Inky Digital">
            </a>
            <h1>404</h1>
            <h2><b>Página não encontrada!</b></h2>
            <p>P a r e c e &nbsp;&nbsp; q u e &nbsp;&nbsp; v o c ê &nbsp;&nbsp; e s t á &nbsp;&nbsp; p e r d i d o
                &nbsp;&nbsp; n o &nbsp;&nbsp; e s p a ç o . . .</p>
        </div>
        <a href="inicio" class="btn-go-home">Voltar para o Início</a>
    </div>
    <div class="objects">
        <img class="object_rocket" src="img/rocket.svg" width="40px">
        <div class="earth-moon">
            <img class="object_earth" src="img/earth.svg" width="100px">
            <img class="object_moon" src="img/moon.svg" width="80px">
        </div>
        <div class="box_astronaut">
            <img class="object_astronaut" src="img/astronaut.svg" width="140px">
        </div>
    </div>
    <div class="glowing_stars">
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
        <div class="star"></div>
    </div>
</div>

</body>
</html>
