<header>
    <div class="inky-nav header-links d-none d-sm-block d-md-none d-lg-block">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="inky-logo">
                    <a href="inicio" class="navbar-brand" title="Logo Inky Digital">
                        <figure class="inky-logo-content"></figure>
                    </a>
                </div>

                <div class="navbar-content">
                    <nav>
                        <ul class="nav d-flex justify-content-between">
                            <li><a class="py-2 px-4 active" href="inicio">Início</a></li>
                            <li><a class="py-2 px-4" href="servicos">Serviços</a></li>
                            <li><a class="py-2 px-4" href="contato">Contato</a></li>
                        </ul>
                    </nav>
                </div>

                <div class="d-flex justify-content-end social icons-content">
                    <a target="_blank" class="d-flex" rel="noreferrer" title="Ícone Facebook" href="https://www.facebook.com/inkydigitalsolucoes" >
                        <i class="fab fa-facebook-f icone-color"></i>
                    </a>
                    <a target="_blank" class="d-flex pl-4" rel="noreferrer" title="Ícone Instagram" href="https://www.instagram.com/inkydigitalsolucoes/">
                        <i class="fab fa-instagram icone-color"></i>
                    </a>
                    <a target="_blank" class="d-flex pl-4" rel="noreferrer" title="Ícone Whatsapp" href="https://api.whatsapp.com/send?phone=5547999328390&text=">
                        <i style="color: #24cb62 !important;" class="fab fa-whatsapp"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="border-adapter find d-none d-sm-block d-md-block d-lg-none d-block">

        <div id="mySidenav" class="sidenav">

            <br/>

            <div class="menu-logo layout horizontal center justified">
                <div class="mobile-item text-left">
                    <img src="img/logo-white.svg" alt="Logo Inky Digital" height="25">
                </div>

                <div class="menu-horizontal layout vertical">

                    <a href="inicio" onclick="closeNav()" class="layout horizontal" style="margin-top: 30px;">
                        <span>Início</span>
                        <div class="bar-inky d-flex m-0 ml-2 mt-2"></div>
                    </a>

                    <a href="servicos" onclick="closeNav()" class="layout horizontal">
                        <span>Serviços</span>
                    </a>

                    <a href="contato" onclick="closeNav()" class="layout horizontal">
                        <span>Contato</span>
                    </a>

                    <div class="d-flex social-content ml-2">
                        <a target="_blank" class="d-flex" rel="noreferrer" href="https://www.facebook.com/inkydigitalsolucoes" >
                            <i class="fab fa-facebook-f text-white"></i>
                        </a>
                        <a target="_blank" class="d-flex pl-4" rel="noreferrer" href="https://www.instagram.com/inkydigitalsolucoes/">
                            <i class="fab fa-instagram text-white"></i>
                        </a>
                        <a target="_blank" class="d-flex pl-4" rel="noreferrer" href="https://api.whatsapp.com/send?phone=554799328390&text=Ol%C3%A1!%20Gostaria%20de%20conversar%20sobre%20um%20projeto.">
                            <i style="color: #24cb62 !important;" class="fab fa-whatsapp"></i>
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <div id="menu-btn" class="menu" onclick="openNav(this);" opened="0">
            <span class="bar1"></span>
            <span class="bar2"></span>
            <span class="bar3"></span>
        </div>

    </div>
</header>