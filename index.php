<!DOCTYPE html>
<html lang="pt-br">

<?php
require_once('head.php');
?>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T868PHZ"
            height="0" width="0" style="display:none;visibility:hidden">
    </iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<?php
require_once('header.php');
?>

<div id="fullpage">
    <?php
    require_once('pages/header-content.php');
    require_once('pages/quem_somos.php');
    require_once('pages/servicos.php');
    require_once('pages/vantagens.php');
    require_once('footer.php');
    ?>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"
        integrity="sha512-7x3zila4t2qNycrtZ31HO0NnJr8kg2VI67YLoRSyi9hGhRN66FHYWr7Axa9Y1J9tGYHVBPqIjSE1ogHrJTz51g=="
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous" async></script>
<script src="js/jquery.fullpage.min.js" async></script>
<script src="js/fullpage.min.js" async></script>
<script src="js/inky-content-js.min.js" async></script>
<script src="js/inky-baseline-jquery.min.js" defer></script>
<script src="js/inky-content-js.min.js" async></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js" async></script>

<script>
    // BOTÃO ENVIO

    $('.activate').on('click touch', function (e) {
        var self = $(this);
        if (!self.hasClass('loading')) {
            self.addClass('loading');
            setTimeout(function () {
                self.addClass('done');
                setTimeout(function () {
                    self.removeClass('loading done');
                }, 1600);
            }, 3200);
        }
    });


    $(document).on('submit', '#formContato', function () {
        $("input").val("");
        $("textarea").val("");
    });
</script>
<script>
    feather.replace();

    // ENVIO DE E-MAIL

    function closeInsc() {
        $("#sendingEmail").fadeOut("fast");
    }

    var form = $('#formContato');

    form.submit(function (event) {

        event.preventDefault();

        $.ajax({
            type: "POST",
            url: "sendmail.php", /* endereço do script PHP */
            async: true,
            data: form.serialize(), /* informa Url */
            success: function (data) { /* sucesso */

                if (data != 1) {

                    console.error(data);

                } else {

                    $("#sendingEmail").fadeIn("fast");

                }

            },
        });
    });
</script>
<script>
    // Mobile menu

    $('.menu').click(function () {
        $(this).toggleClass('active');
        $(this).toggleClass('open');
        $('.menu-mobile').toggleClass('active')
        $('.menu-mobile').toggleClass('open')
    });

    const observer = lozad(); // lazy loads elements with default selector as '.lozad'
    observer.observe();
</script>

</body>
</html>