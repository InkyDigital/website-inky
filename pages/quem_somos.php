<section class="inky-scrolling about-content position-relative">
    <div class="background-overlay position-absolute h-100 w-100"></div>
    <div class="container h-100 d-flex justify-content-center about-content-ajust align-items-center text-center">

        <div class="row justify-content-between align-items-center">
            <div class="col-md-5">
                <div class="text-content inky-title">
                    <h2>Quem Somos</h2>
                    <span class="bar-inky d-flex"></span>
                    <p class="inky-description">
                        A Inky Digital é uma agência digital de soluções tecnológicas conhecida por oferecer produtos de
                        alto desempenho e um design moderno e inovador. <br><br>
                        Prezamos por desenvolver projetos originais e exclusivos de acordo com o perfil e os objetivos
                        de cada cliente.
                    </p>

                    <div class="inky-msg-content d-flex justify-content-between mt-5">
                        <div class="icon-wrapper shadow-primary">
                            <div style="background-color: #4728f7" class="icon-blur"></div>
                            <div class="icon icon-primary"><span data-feather="globe"></span></div>
                        </div>
                        <div class="text-content">
                            <h3 class="text-white">Tenha sucesso no mundo digital!</h3>
                            <p class="m-0">transformamos suas ideias em soluções extraordinárias!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>