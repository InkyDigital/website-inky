<section class="inky-scrolling py-5 services-page">

    <div class="container justify-content-center align-items-center text-center h-100vh">
        <div class="row justify-content-center mb-6">
            <div class="text-content inky-title text-center mt-5">
                <h2 class="fs-3 fs-lg-5 lh-sm mb-3 mt-4">Nossas Especialidades</h2>
                <span class="bar-inky d-flex mx-auto"></span>
                <p class="mb-0">Ferramentas profissionais e modernas para a sua empresa.</p>
            </div>
        </div>
        <div class="row circle-blend circle-blend-right circle-cyan mt-5">
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-danger">
                        <div style="background-color: #EB6453" class="icon-blur"></div>
                        <div class="icon icon-danger"><span data-feather="layout"></span></div>
                    </div>
                    <h3 class="mt-3">Websites
                        Personalizados</h3>
                    <p class="mb-md-0">Site, hotsite, blog ou loja virtual otimizados e com um design único e
                        personalizado.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-primary">
                        <div style="background-color: #4E92F9" class="icon-blur"></div>
                        <div class="icon icon-primary"><span data-feather="smartphone"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">Aplicativos & Softwares
                    </h3>
                    <p class="mb-md-0">Ferramenta de negócios, atuando na atração do
                        seu público e geração de oportunidades de vendas.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-warning">
                        <div style="background-color: #EFAB18" class="icon-blur"></div>
                        <div class="icon icon-warning"><span data-feather="edit-2"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">Identidade
                        Visual</h3>
                    <p class="mb-md-0">Construa uma imagem de marca sólida que reúna embasamento
                        conceitual visando os valores e ao público de sua companhia.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-success">
                        <div style="background-color: #96D121" class="icon-blur"></div>
                        <div class="icon icon-success"><span data-feather="printer"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">Design
                        Gráfico</h3>
                    <p class="mb-md-0">Trabalhamos com todo o posicionamento da sua marca, desenvolvendo peças gráficas
                        com estratégia.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-secondary">
                        <div style="background-color: #4E92F9" class="icon-blur"></div>
                        <div class="icon icon-primary"><span data-feather="facebook"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">
                        Artes para Redes Sociais</h3>
                    <p class="mb-md-0">Gere mais leads para sua empresa
                        através das artes personalizadas, buscando os melhores
                        resultados para seus clientes.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-info">
                        <div style="background-color: #21D4DF" class="icon-blur"></div>
                        <div class="icon icon-info"><span data-feather="box"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">Modelagens e
                        Animações em 3D</h3>
                    <p class="mb-md-0">Modelagem de objetos e elementos em 3D rico em detalhes.
                        impressione seus clientes mostrando seu projeto ou produto tridimensional.</p>
                </div>
            </div>
        </div>
    </div>
</section>