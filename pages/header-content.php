<section class="inky-scrolling header-content">
    <div class="container h-100 d-flex justify-content-center align-items-center text-center">
        <div class="row">
            <div class=col-12">
                <figure class="figure-content"></figure>

                <div class="text-content">
                    <p class="m-0">S o l u ç õ e s&nbsp; &nbsp; t e c n o l ó g i c a s&nbsp; &nbsp; q u e</p>
                    <h1>Transformam</h1>
                </div>
            </div>
        </div>

        <div class="down-arrow-content position-absolute" style="bottom: 20px">
            <lottie-player class="mx-auto" src="img/icons/arrow.json" background="transparent"
                           speed="1" style="width: 300px; height: 100px;" loop autoplay></lottie-player>
        </div>
    </div>
</section>