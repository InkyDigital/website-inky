<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Inky Digital | Serviços</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Soluções Tecnológicas que Transformam!">
    <meta name="author" content="Inky Digital">
    <meta name="keywords"
          content="Website, mobile, aplicações, web, inky digital, criar website, criar aplicativo, bot, desenvolvimento, software, aplicativo, app, android, ios, personalizado, site, inteligencia artificial, automação, nativo, react, wordpress, modelagem 3D, 3D, identidade visual, marca, logo, social media, redes sociais, gerenciamento, design gráfico">
    <meta name="robots" content="index">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="pt-BR">
    <meta name="generator" content="N/A">
    <meta charset="utf-8">
    <link rel="stylesheet" href='css/inky-base.css'>
    <link rel="stylesheet" href='css/mobile-menu.css'>
    <link rel="stylesheet" href="css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="css/theme.min.css">
    <link rel="shortcut icon" href="img/logo-icon.svg">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous"/>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css"/>
    <link rel="stylesheet" href="slick/slick.css">
    <link rel="stylesheet" href="slick/slick-theme.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>
    <script src="js/jquery.fullpage.min.js"></script>
    <script src="js/fullpage.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"
            integrity="sha512-7x3zila4t2qNycrtZ31HO0NnJr8kg2VI67YLoRSyi9hGhRN66FHYWr7Axa9Y1J9tGYHVBPqIjSE1ogHrJTz51g=="
            crossorigin="anonymous"></script>
    <script src="js/inky-content-js.min.js"></script>
    <script src="js/inky-baseline-jquery.min.js"></script>
    <script src="js/inky-content-js.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <script src="slick/slick.min.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-194032811-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-194032811-1');
    </script>
    <script>
        window.addEventListener("load", function () {
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "rgba(80, 78, 93, 0.40)",
                        "text": "#fff",
                    },
                    "button": {
                        "background": "#4B28F7",
                    }
                },
                "position": "bottom-right",
                "content": {
                    "message": "Esse website utiliza cookies para garantir que você tenha a melhor experiência possível enquanto navega.",
                    "dismiss": "Entendi!",
                    "link": "Saiba Mais",
                    "href": "politica-de-privacidade"
                }
            })
        });
    </script>
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 2367640, hjsv: 6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
</head
<body>
<style>
    .footer-content{
        height: auto !important;
    }

    footer{
        position: relative;
    }
</style>
<header>
    <div class="inky-nav header-links d-none d-sm-block d-md-none d-lg-block header-pag">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="inky-logo">
                    <a href="inicio" class="navbar-brand" title="Logo Inky Digital">
                        <figure style="background-image: url('img/logo-white.svg');" class="inky-logo-content"></figure>
                    </a>
                </div>

                <div class="navbar-content">
                    <nav>
                        <ul class="nav d-flex justify-content-between">
                            <li><a class="py-2 px-4" href="inicio">Início</a></li>
                            <li><a class="py-2 px-4 active" href="servicos">Serviços</a></li>
                            <li><a class="py-2 px-4" href="contato">Contato</a></li>
                        </ul>
                    </nav>
                </div>

                <div class="d-flex justify-content-end social icons-content">
                    <a target="_blank" class="d-flex" rel="noreferrer" title="Ícone Facebook"
                       href="https://www.facebook.com/inkydigitalsolucoes">
                        <i class="fab fa-facebook-f icone-color"></i>
                    </a>
                    <a target="_blank" class="d-flex pl-4" rel="noreferrer" title="Ícone Instagram"
                       href="https://www.instagram.com/inkydigitalsolucoes">
                        <i class="fab fa-instagram icone-color"></i>
                    </a>
                    <a target="_blank" class="d-flex pl-4" rel="noreferrer" title="Ícone Whatsapp"
                       href="https://api.whatsapp.com/send?phone=554799328390&text=Ol%C3%A1!%20Gostaria%20de%20conversar%20sobre%20um%20projeto.">
                        <i style="color: #24cb62 !important;" class="fab fa-whatsapp"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="border-adapter find d-none d-sm-block d-md-block d-lg-none d-block">

        <div id="mySidenav" class="sidenav">

            <br/>

            <div class="menu-logo layout horizontal center justified">
                <div class="mobile-item text-left">
                    <img src="img/logo-white.svg" alt="Logo Inky Digital" height="25">
                </div>

                <div class="menu-horizontal layout vertical">

                    <a href="inicio" onclick="closeNav()" class="layout horizontal" style="margin-top: 30px;">
                        <span>Início</span>
                    </a>

                    <a href="servicos" onclick="closeNav()" class="layout horizontal">
                        <span>Serviços</span>
                        <div class="bar-inky d-flex m-0 ml-2 mt-2"></div>
                    </a>

                    <a href="contato" onclick="closeNav()" class="layout horizontal">
                        <span>Contato</span>
                    </a>

                    <div class="d-flex social-content ml-2">
                        <a target="_blank" class="d-flex" rel="noreferrer"
                           href="https://www.facebook.com/inkydigitalsolucoes">
                            <i class="fab fa-facebook-f text-white"></i>
                        </a>
                        <a target="_blank" class="d-flex pl-4" rel="noreferrer"
                           href="https://www.instagram.com/inkydigitalsolucoes">
                            <i class="fab fa-instagram text-white"></i>
                        </a>
                        <a target="_blank" class="d-flex pl-4" rel="noreferrer"
                           href="https://api.whatsapp.com/send?phone=554799328390&text=Ol%C3%A1!%20Gostaria%20de%20conversar%20sobre%20um%20projeto.">
                            <i style="color: #24cb62 !important;" class="fab fa-whatsapp"></i>
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <div id="menu-btn" class="menu" onclick="openNav(this);" opened="0">
            <span class="bar1"></span>
            <span class="bar2"></span>
            <span class="bar3"></span>
        </div>

    </div>
</header>

<section class="work-page-content p-0">
    <div class="container d-flex justify-content-center align-items-center text-center h-100vh">
        <div class="row mt-5 pt-5">
            <div class="col mt-4">

                <div class="text-content text-center">
                    <h2>Fazemos<br> A c o n t e c e r!</h2>
                    <span class="bar-inky d-flex mx-auto"></span>
                    <p>
                        Nossa equipe é especializada nas melhores ferramentas e linguagens de programação do mercado
                        <br> para
                        entregar os melhores produtos para você! <br><br>

                        Utilizamos as mesmas ferramentas e linguagens de empresas como:
                    </p>
                </div>
            </div>

            <div class="box-brands col-md-12 d-flex justify-content-between align-items-center">

                <div>
                    <a target="_blank" href="https://www.facebook.com/" rel="noreferrer" title="Logo Facebook"
                       class="box-brands">
                        <img src="img/brands/facebook.svg" alt="Logo Facebook">
                    </a>
                </div>


                <div>
                    <a target="_blank" href="https://www.instagram.com/" rel="noreferrer" title="Logo Instagram"
                       class="box-brands">
                        <img src="img/brands/instagram.svg" alt="Logo Instagram">
                    </a>
                </div>


                <div>
                    <a target="_blank" href="https://www.ubereats.com/" rel="noreferrer" title="Logo Uber Eats"
                       class="box-brands">
                        <img src="img/brands/uber_eats.svg" alt="Logo Uber Eats">
                    </a>
                </div>


                <div>
                    <a target="_blank" href="https://www.airbnb.com.br/" rel="noreferrer" title="Logo Airbnb"
                       class="box-brands">
                        <img src="img/brands/airbnb.svg" alt="Logo Airbnb">
                    </a>
                </div>


                <div>
                    <a target="_blank" href="https://www.walmart.com/" rel="noreferrer" title="Logo Walmart" class="box-brands">
                        <img src="img/brands/walmart.svg" alt="Logo Walmart">
                    </a>
                </div>

            </div>
        </div>

        <div class="down-arrow-content position-absolute" style="bottom: 20px">
            <lottie-player class="mx-auto" src="img/icons/arrow.json" background="transparent"
                           speed="1" style="width: 300px; height: 100px;" loop autoplay></lottie-player>
        </div>
    </div>
</section>

<div class="contariner mobile-carousel">
    <div class="brands-carousel">

        <div>
            <a target="_blank" href="https://www.facebook.com/" rel="noreferrer" title="Logo Facebook"
               class="box-brands">
                <img src="img/brands/facebook.svg" alt="Logo Facebook">
            </a>
        </div>


        <div>
            <a target="_blank" href="https://www.instagram.com/" rel="noreferrer" title="Logo Instagram"
               class="box-brands">
                <img src="img/brands/instagram.svg" alt="Logo Instagram">
            </a>
        </div>


        <div>
            <a target="_blank" href="https://www.ubereats.com/" rel="noreferrer" title="Logo Uber Eats"
               class="box-brands">
                <img src="img/brands/uber_eats.svg" alt="Logo Uber Eats">
            </a>
        </div>


        <div>
            <a target="_blank" href="https://www.airbnb.com.br/" rel="noreferrer" title="Logo Airbnb"
               class="box-brands">
                <img src="img/brands/airbnb.svg" alt="Logo Airbnb">
            </a>
        </div>


        <div>
            <a target="_blank" href="https://www.walmart.com/" rel="noreferrer" title="Logo Walmart" class="box-brands">
                <img src="img/brands/walmart.svg" alt="Logo Walmart">
            </a>
        </div>

    </div>
</div>

<section class="services-page">

    <div class="container justify-content-center align-items-center text-center">
        <div class="row justify-content-center mb-6">
            <div class="text-content inky-title text-center mt-5 pt-5">
                <h2 class="fs-3 fs-lg-5 lh-sm mb-3 mt-5">Nossas Especialidades</h2>
                <p class="mb-0">Ferramentas profissionais e modernas para a sua empresa.</p>
            </div>
        </div>
        <div class="row circle-blend circle-blend-right circle-cyan mt-5">
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-danger">
                        <div style="background-color: #EB6453" class="icon-blur"></div>
                        <div class="icon icon-danger"><span data-feather="layout"></span></div>
                    </div>
                    <h3 class="mt-3">Websites
                        Personalizados</h3>
                    <p class="mb-md-0">Site, hotsite, blog ou loja virtual otimizados e com um design único e
                        personalizado.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-primary">
                        <div style="background-color: #4E92F9" class="icon-blur"></div>
                        <div class="icon icon-primary"><span data-feather="smartphone"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">Aplicativos & Softwares
                    </h3>
                    <p class="mb-md-0">Ferramenta de negócios, atuando na atração do
                        seu público e geração de oportunidades de vendas.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-warning">
                        <div style="background-color: #EFAB18" class="icon-blur"></div>
                        <div class="icon icon-warning"><span data-feather="edit-2"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">Identidade
                        Visual</h3>
                    <p class="mb-md-0">Construa uma imagem de marca sólida que reúna embasamento
                        conceitual visando os valores e ao público de sua companhia.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-success">
                        <div style="background-color: #96D121" class="icon-blur"></div>
                        <div class="icon icon-success"><span data-feather="printer"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">Design
                        Gráfico</h3>
                    <p class="mb-md-0">Trabalhamos com todo o posicionamento da sua marca, desenvolvendo peças gráficas
                        com estratégia.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-secondary">
                        <div style="background-color: #4E92F9" class="icon-blur"></div>
                        <div class="icon icon-primary"><span data-feather="facebook"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">
                        Artes para Redes Sociais</h3>
                    <p class="mb-md-0">Gere mais leads para sua empresa
                        através das artes personalizadas, buscando os melhores
                        resultados para seus clientes.</p>
                </div>
            </div>
            <div class="col-md-4 mb-6">
                <div class="text-center px-lg-3">
                    <div class="icon-wrapper shadow-info">
                        <div style="background-color: #21D4DF" class="icon-blur"></div>
                        <div class="icon icon-info"><span data-feather="box"></span></div>
                    </div>
                    <h3 class="mt-3 fw-bold">Modelagens e
                        Animações em 3D</h3>
                    <p class="mb-md-0">Modelagem de objetos e elementos em 3D rico em detalhes.
                        impressione seus clientes mostrando seu projeto ou produto tridimensional.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $('.brands-carousel').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 1
                }
            }]
        });
    });
</script>

<?php

require '../footer.php';

?>
</body>