<section class="inky-scrolling benefits-content">
    <div class="container h-100 d-flex justify-content-center align-items-center text-center">

        <div class="row justify-content-between align-items-center w-100">
            <div class="col-md-6" style="pointer-events: none">
                <object type="image/svg+xml" title="Ilustração de Banco de Dados."
                        data="img/database.svg">
                    Ilustração de Banco de Dados.
                </object>
            </div>


            <div class="col-md-5">
                <div class="text-content inky-title text-left">
                    <h2>Mais Segurança e Velocidade</h2>
                    <span class="bar-inky d-flex"></span>
                    <p class="inky-description">
                        Utilizamos o servidor do Google Cloudflare para melhor
                        segurança e velocidade. <br><br>

                        Garanta a segurança e dos usuários que navegam em seu site, aplicativo ou sistema.
                    </p>

                    <img style="height: 80px" class="w-100 lozad" data-src="img/cloudflare.svg">
                </div>
            </div>
        </div>

    </div>
</section>

<section class="inky-scrolling benefits-content benefits-bottom">
    <div class="container h-100 d-flex justify-content-center align-items-center text-center">

        <div class="row justify-content-between align-items-center ajust-mobile">

            <div class="col-md-5">
                <div class="text-content inky-title text-left">
                    <h2>Soluções perfeitas <br>
                        para todas as plataformas!</h2>
                    <span class="bar-inky d-flex"></span>
                </div>
            </div>

            <div class="col-md-7">
                <img class="w-100 h-100" src="img/plataformas.webp" alt="Imagem Dispositivos Móveis">
            </div>

        </div>
    </div>
</section>