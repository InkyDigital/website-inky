<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Inky Digital | Quem Somos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Soluções Tecnológicas que Transformam!">
    <meta name="author" content="Inky Digital">
    <meta name="keywords"
          content="Website, mobile, aplicações, web, inky digital, criar website, criar aplicativo, bot, desenvolvimento, software, aplicativo, app, android, ios, personalizado, site, inteligencia artificial, automação, nativo, react, wordpress, modelagem 3D, 3D, identidade visual, marca, logo, social media, redes sociais, gerenciamento, design gráfico">
    <meta name="robots" content="index">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="pt-BR">
    <meta name="generator" content="N/A">
    <meta charset="utf-8">
    <link rel="stylesheet" href='css/inky-base.css'>
    <link rel="stylesheet" href='css/mobile-menu.css'>
    <link rel="stylesheet" href="css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="stylesheet" href="css/theme.min.css">
    <link rel="shortcut icon" href="img/logo-icon.svg">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous"/>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css"/>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>
    <script src="js/jquery.fullpage.min.js"></script>
    <script src="js/fullpage.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"
            integrity="sha512-7x3zila4t2qNycrtZ31HO0NnJr8kg2VI67YLoRSyi9hGhRN66FHYWr7Axa9Y1J9tGYHVBPqIjSE1ogHrJTz51g=="
            crossorigin="anonymous"></script>
    <script src="js/inky-content-js.min.js"></script>
    <script src="js/inky-baseline-jquery.min.js"></script>
    <script src="js/inky-content-js.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-194032811-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-194032811-1');
    </script>
    <script>
        window.addEventListener("load", function () {
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "rgba(80, 78, 93, 0.40)",
                        "text": "#fff",
                    },
                    "button": {
                        "background": "#4B28F7",
                    }
                },
                "position": "bottom-right",
                "content": {
                    "message": "Esse website utiliza cookies para garantir que você tenha a melhor experiência possível enquanto navega.",
                    "dismiss": "Entendi!",
                    "link": "Saiba Mais",
                    "href": "politica-de-privacidade"
                }
            })
        });
    </script>
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 2367640, hjsv: 6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
</head>

<body>
<header>
    <div class="inky-nav header-links d-none d-sm-block d-md-none d-lg-block header-pag">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="inky-logo">
                    <a href="inicio" class="navbar-brand" title="Logo Inky Digital">
                        <figure style="background-image: url('img/logo-white.svg');" class="inky-logo-content"></figure>
                    </a>
                </div>

                <div class="navbar-content">
                    <nav>
                        <ul class="nav d-flex justify-content-between">
                            <li><a class="py-2 px-4" href="inicio">Início</a></li>
                            <li><a class="py-2 px-4" href="servicos">Serviços</a></li>
                            <li><a class="py-2 px-4" href="contato">Contato</a></li>
                        </ul>
                    </nav>
                </div>

                <div class="d-flex justify-content-end social icons-content">
                    <a target="_blank" class="d-flex" rel="noreferrer" title="Ícone Facebook" href="https://www.facebook.com/inkydigitalsolucoes" >
                        <i class="fab fa-facebook-f icone-color"></i>
                    </a>
                    <a target="_blank" class="d-flex pl-4" rel="noreferrer" title="Ícone Instagram" href="https://www.instagram.com/inkydigitalsolucoes">
                        <i class="fab fa-instagram icone-color"></i>
                    </a>
                    <a target="_blank" class="d-flex pl-4" rel="noreferrer" title="Ícone Whatsapp" href="https://api.whatsapp.com/send?phone=554799328390&text=Ol%C3%A1!%20Gostaria%20de%20conversar%20sobre%20um%20projeto.">
                        <i style="color: #24cb62 !important;" class="fab fa-whatsapp"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="border-adapter find d-none d-sm-block d-md-block d-lg-none d-block">

        <div id="mySidenav" class="sidenav">

            <br/>

            <div class="menu-logo layout horizontal center justified">
                <div class="mobile-item text-left">
                    <img src="img/logo-white.svg" alt="Logo Inky Digital" height="25">
                </div>

                <div class="menu-horizontal layout vertical">

                    <a href="inicio" onclick="closeNav()" class="layout horizontal" style="margin-top: 30px;">
                        <span>Início</span>
                    </a>

                    <a href="servicos" onclick="closeNav()" class="layout horizontal">
                        <span>Serviços</span>
                    </a>

                    <a href="contato" onclick="closeNav()" class="layout horizontal">
                        <span>Contato</span>
                    </a>

                    <div class="d-flex social-content ml-2">
                        <a target="_blank" class="d-flex" rel="noreferrer" href="https://www.facebook.com/inkydigitalsolucoes" >
                            <i class="fab fa-facebook-f text-white"></i>
                        </a>
                        <a target="_blank" class="d-flex pl-4" rel="noreferrer" href="https://www.instagram.com/inkydigitalsolucoes">
                            <i class="fab fa-instagram text-white"></i>
                        </a>
                        <a target="_blank" class="d-flex pl-4" rel="noreferrer" href="https://api.whatsapp.com/send?phone=554799328390&text=Ol%C3%A1!%20Gostaria%20de%20conversar%20sobre%20um%20projeto.">
                            <i style="color: #24cb62 !important;" class="fab fa-whatsapp"></i>
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <div id="menu-btn" class="menu" onclick="openNav(this);" opened="0">
            <span class="bar1"></span>
            <span class="bar2"></span>
            <span class="bar3"></span>
        </div>

    </div>
</header>

<section class="about-page-content" style="height: 100vh;">
    <div class="container h-100 d-flex justify-content-center align-items-center text-center">

        <div class="d-flex justify-content-between align-items-center">
            <div class="text-content text-white">
                 A Inky Digital é uma agência digital de soluções tecnológicas conhecida por oferecer produtos de
                alto desempenho e um design moderno e inovador. <br><br>
                Atualmente, a empresa conta com uma equipe de jovens integrantes que, com muito planejamento e
                pesquisa, elaboram projetos inovadores, dinâmicos e totalmente personalizados, prezamos por
                desenvolver projetos originais e exclusivos de acordo com o perfil e os objetivos de cada
                cliente.<br><br>
            </div>
        </div>

        <div class="down-arrow-content position-absolute" style="bottom: 20px">
            <lottie-player class="mx-auto" src="img/icons/arrow.json" background="transparent"
                           speed="1" style="width: 300px; height: 100px;" loop autoplay></lottie-player>
        </div>

    </div>
</section>

<?php

require '../footer.php';

?>
</body>
</html>