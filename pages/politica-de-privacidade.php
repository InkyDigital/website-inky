<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Inky Digital | Política de Privacidade</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Soluções Tecnológicas que Transformam!">
    <meta name="author" content="Inky Digital">
    <meta name="keywords"
          content="Website, mobile, aplicações, web, inky digital, criar website, criar aplicativo, bot, desenvolvimento, software, aplicativo, app, android, ios, personalizado, site, inteligencia artificial, automação, nativo, react, wordpress, modelagem 3D, 3D, identidade visual, marca, logo, social media, redes sociais, gerenciamento, design gráfico">
    <meta name="robots" content="index">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="pt-BR">
    <meta name="generator" content="N/A">
    <meta charset="utf-8">
    <link rel="stylesheet" href='css/inky-base.css'>
    <link rel="stylesheet" href="css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="shortcut icon" href="img/logo-icon.svg">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
          crossorigin="anonymous"/>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</head>
<body>
<section class="privacy-politic">
    <div class="container">
        <div class="termos">

            <h2>Política de privacidade <a href='inicio'>Inky Digital</a></h2>
            <p>Todas as suas informações pessoais
                recolhidas, serão usadas para o ajudar a tornar a sua visita no nosso site o mais produtiva e agradável
                possível.</p>
            <p>A garantia da confidencialidade dos dados pessoais dos utilizadores do nosso site é importante para
                a Inky Digital.</p>
            <p>Todas as informações pessoais relativas a membros, assinantes, clientes ou visitantes que usem o
                website da Inky Digital serão tratadas em concordância com a Lei da Proteção de Dados Pessoais de 26 de outubro
                de 1998 (Lei
                n.º
                67/98).</p>
            <p>A informação pessoal recolhida pode incluir o seu nome, e-mail, número de telefone e/ou telemóvel,
                morada, data de nascimento e/ou outros.</p>
            <p>O uso do site da Inky Digital pressupõe a aceitação deste acordo de privacidade. A
                equipe da Inky Digital reserva-se ao direito de alterar este acordo sem aviso prévio. Deste modo, recomendamos
                que
                consulte a nossa política de privacidade com regularidade de forma a estar sempre atualizado.</p>
            <h2>Os
                anúncios</h2>
            <p>Tal como outros websites, coletamos e utilizamos informação contida nos anúncios. A informação
                contida nos anúncios, inclui o seu endereço IP (Internet Protocol), o seu ISP (Internet Service Provider, como o
                Sapo, Clix, ou outro), o browser que utilizou ao visitar o nosso website (como o Internet Explorer ou o
                Firefox), o
                tempo da sua visita e que páginas visitou dentro do nosso website.</p>
            <h2>Cookie DoubleClick Dart</h2>
            <p>O Google,
                como fornecedor de terceiros, utiliza cookies para exibir anúncios no nosso website;</p>
            <p>Com o cookie DART, o
                Google pode exibir anúncios com base nas visitas que o leitor fez a outros websites na Internet;</p>
            <p>Os
                utilizadores podem desativar o cookie DART visitando a Política de <a href='http://politicaprivacidade.com/'
                                                                                      title='privacidade da rede de conteúdo'>privacidade
                    da rede de conteúdo</a> e dos anúncios do Google.</p>
            <h2>Os Cookies e Web Beacons</h2>
            <p>Utilizamos cookies para
                armazenar informação, tais como as suas preferências pessoas quando visita o nosso website. Isto poderá incluir
                um
                simples popup, ou uma ligação em vários serviços que providenciamos, tais como fóruns.</p>
            <p>Em adição também
                utilizamos publicidade de terceiros no nosso website para suportar os custos de manutenção. Alguns destes
                publicitários, poderão utilizar tecnologias como os cookies e/ou web beacons quando publicitam no nosso website,
                o
                que fará com que esses publicitários (como o Google através do Google AdSense) também recebam a sua informação
                pessoal, como o endereço IP, o seu ISP, o seu browser, etc. Esta função é geralmente utilizada para geotargeting
                (mostrar publicidade de Lisboa apenas aos leitores oriundos de Lisboa por ex.) ou apresentar publicidade
                direcionada
                a um tipo de utilizador (como mostrar publicidade de restaurante a um utilizador que visita sites de culinária
                regularmente, por ex.).</p>
            <p>Você detém o poder de desligar os seus cookies, nas opções do seu browser, ou
                efetuando alterações nas ferramentas de programas Anti-Virus, como o Norton Internet Security. No entanto, isso
                poderá alterar a forma como interage com o nosso website, ou outros websites. Isso poderá afetar ou não permitir
                que
                faça logins em programas, sites ou fóruns da nossa e de outras redes.</p>
            <h2>Ligações a Sites de terceiros</h2>
            <p>A
                Inky Digital possui ligações para outros sites, os quais, a nosso ver, podem conter informações / ferramentas
                úteis
                para
                os nossos visitantes. A nossa política de privacidade não é aplicada a sites de terceiros, pelo que, caso visite
                outro site a partir do nosso deverá ler a politica de privacidade do mesmo.</p>
            <p>Não nos responsabilizamos pela
                política de privacidade ou conteúdo presente nesses mesmos sites.</p>

        </div>
    </div>
</section>
</body>
</html>